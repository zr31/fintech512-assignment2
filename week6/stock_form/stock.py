from flask import Blueprint
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
import requests
from flask import url_for
from werkzeug.exceptions import abort

from stock_form.db import get_db

bp = Blueprint("stock", __name__)

def get_prices(stock_symbol):  
    url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={stock_symbol}&apikey=SL0PHEWZOZRVMKQA'    
    response = requests.get(url)
    data = response.json()
    days = data["Time Series (Daily)"]
    keys = sorted(days.keys(),reverse=True)
    daily= {"current": days[keys[0]]["4. close"], 
            "volume": days[keys[0]]["5. volume"], 
            "previous": days[keys[1]]["4. close"], 
            "open": days[keys[0]]["1. open"]}
    return daily

@bp.route("/")
def index():
    """Show all the stocks, most recent first."""
    db = get_db()
    stocks = db.execute(
        "SELECT * "
        " FROM stock_tracker sp"
        " ORDER BY created ASC"
    ).fetchall()

    return render_template("stock/index.html", stocks=stocks)

@bp.route("/stocks/bar", methods=("GET",))
def getStocksBar():
    """Show all the stocks, most recent first."""
    db = get_db()
    stocks = db.execute(
        "SELECT * "
        " FROM stock_tracker sp"
        " ORDER BY created ASC"
    ).fetchall()

    y = []
    x = []
    
    for i in range(len(stocks)):
        y.append(stocks[i][1])
        x.append(stocks[i][3])

    stock_dict = {}
    stock_dict['y'] = y
    stock_dict['x'] = x
    stock_dict['type'] = 'bar'
    stock_dict['orientation'] = 'h'

    return stock_dict

@bp.route("/stocks/pie", methods=("GET",))
def getStocksPie():
    """Show all the stocks, most recent first."""
    db = get_db()
    stocks = db.execute(
        "SELECT * "
        " FROM stock_tracker sp"
        " ORDER BY created ASC"
    ).fetchall()

    values = []
    labels = []
    
    for i in range(len(stocks)):
        labels.append(stocks[i][1])
        values.append(stocks[i][4] * stocks[i][5])

    stock_dict = {}
    stock_dict['labels'] = labels
    stock_dict['values'] = values
    stock_dict['type'] = 'pie'

    return stock_dict


@bp.route("/stocks", methods=("POST",))
def create():
    """Create a new stock track """
    symbol = request.form["stock-symbol"]
    price = request.form["stock-price"]
    shares = request.form["stock-shares"]
    error = None

    current = get_prices(symbol)['current']
    percent_change = ((float(current)-float(price)/float(price)))
    
    db = get_db()
    db.execute(
        "INSERT INTO stock_tracker (stock_symbol, tracking_price, shares, current_price, percent_change) VALUES (?, ?, ?, ?, ?)",
        (symbol, price, shares, current, percent_change),
    )
    db.commit()
    return redirect(url_for("stock.index"))


@bp.route("/<int:id>/delete", methods=("GET",))
def delete(id):
    """Delete a post.
    Ensures that the post exists and that the logged in user is the
    author of the post.
    """
    db = get_db()
    db.execute("DELETE FROM stock_tracker WHERE id = ?", (id,))
    db.commit()
    return redirect(url_for("stock.index"))