var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      /* Do things after DOM has fully loaded */ 
      if (document.getElementById("symbol") != null) {
          makeplot()
      }
  });


function makeplot() {
    const ss = document.getElementById("symbol")
    let url = "/closing?stock-symbol="+ss.textContent
    fetch(url)
    .then((response) => response.json()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((data) => {
      makePlotly(data)
    })
};

function makePlotly(data){
    var traces = [ data ];
    var layout  = { title: document.getElementById("name").textContent + " Stock Price History (Past Year)",
                    yaxis: {rangemode: "tozero"} }
  
    myDiv = document.getElementById('myDiv');
    Plotly.newPlot( myDiv, traces, layout );
};
