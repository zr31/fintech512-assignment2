console.log('HELLO');

var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
    }
    
    ready(() => {
        console.log('ready')
        const selectElement = document.querySelector('#stockselect');
        selectElement.addEventListener('change', (event) => {
            let symbol = event.target.value;
            makeplot(symbol);
        });

        makeplot('AAPL')
    });
    
    function makeplot(symbol) {
        console.log("makeplot: start")
        fetch("includes/data/"+symbol+".csv")
        .then((response) => response.text()) /* asynchronous */
        .catch(error => {
            alert(error)
             })
        .then((text) => {
          console.log("csv: start")
          csv().fromString(text).then(processData)  /* asynchronous */
          console.log("csv: end")
        })
        console.log("makeplot: end")
      
    };
    
    
    function processData(data) {
      console.log("processData: start")
      let x = [], y = []
    
      for (let i=0; i<data.length; i++) {
          row = data[i];
          x.push( row['Date'] );
          y.push( row['Close'] );
      } 
      makePlotly( x, y );
      console.log("processData: end")
    }
    
    function makePlotly( x, y ){
      console.log("makePlotly: start")
      var traces = [{
            x: x,
            y: y
      }];
      var layout  = { title: "Apple Stock Price History"}

      console.log('line 54');
    
      myDiv = document.getElementById('myChart');
      Plotly.newPlot( myDiv, traces, layout );
      console.log("makePlotly: end")
    };