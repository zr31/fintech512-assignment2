var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      /* Do things after DOM has fully loaded */ 
      if (document.getElementById("symbol") != null) {
          console.log('we are ready')
      }
  });

  function makeplot1() {
    console.log("makeplot: start")
    fetch("/stocks/bar")
    .then((data) => {
      return data.json()
    })
    .then(json => {
        makePlotly1(json)
    })
    console.log("makeplot: end")
};


function makePlotly1(json){
    console.log(json)
    var layout = { title: "Bar Chart of Tracking prices",
                    yaxis: {
                        rangemode: "tozero"
                    },
                }
  
    myDiv = document.getElementById('graph1');
    Plotly.newPlot( myDiv, [json], layout );
};


function makeplot2() {
    console.log("makeplot: start")
    fetch("/stocks/pie")
    .then((data) => {
      return data.json()
    })
    .then(json => {
        makePlotly2(json)
    })
    console.log("makeplot: end")
};


function makePlotly2(json){
    console.log(json)
    var layout = { title: "Pie Chart of Shares Times Current Prices" }
  
    myDiv = document.getElementById('graph2');
    Plotly.newPlot( myDiv, [json], layout );
};