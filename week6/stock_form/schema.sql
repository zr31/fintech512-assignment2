-- Initialize the database.
-- Drop any existing data and create empty tables.

DROP TABLE IF EXISTS stock_tracker;

CREATE TABLE stock_tracker (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  stock_symbol VARCHAR NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  tracking_price FLOAT NOT NULL,
  shares INTEGER NOT NULL,
  current_price FLOAT NOT NULL,
  percent_change FLOAT NOT NULL
);