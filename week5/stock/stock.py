from flask import Flask, render_template, request, Blueprint, current_app, flash
import requests
import json
import sys
from stock.config import ALPHA_VANTAGE_API_KEY

bp = Blueprint("stock", __name__)


def get_stock_symbol(request):
    if request.method == 'POST':
        return request.form.get("stock-symbol")
    else:
        return request.args.get("stock-symbol")

@bp.route('/')
def index():
    return render_template('stock/index.html')

@bp.route('/stocks', methods=['GET', 'POST'])
def stocks():
    stock_symbol = get_stock_symbol(request)
    return process_stock_page(stock_symbol)

@bp.route('/stocks/<stock_symbol>', methods=['GET', 'POST'])
def stocks_rest(stock_symbol):
    return process_stock_page(stock_symbol)
    
def process_stock_page(stock_symbol):
    current_app.logger.info(stock_symbol)
    companyData = get_company_data(stock_symbol)
    if companyData == None:
        flash(stock_symbol + " was not found")
        return render_template("stock/index.html")
    daily = get_prices(stock_symbol)
    news = get_new_stories(stock_symbol,5)

    return render_template('stock/stocks.html', companyData=companyData, news=news, daily=daily) 

@bp.route('/closing', methods=['GET', 'POST'])
def prices():
    stock_symbol = get_stock_symbol(request)
    return get_historical_closing_prices(stock_symbol,250)
    

def get_company_data(stock_symbol):
    url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={stock_symbol}&apikey={ALPHA_VANTAGE_API_KEY}'
    print(url,file=sys.stdout)
    response = requests.get(url)
    data = response.json()
    if len(data) == 0:
        return None
    
    result={
            'description': data["Description"],
            'symbol': data.get('Symbol', ''),
            'companyName': data.get('Name', ''),
            'sector': data.get('Sector', ''),
            'industry': data.get('Industry', ''),
            'marketCap': float(data.get('MarketCapitalization', '')),
            'peRatio': data.get('PERatio', ''),
            'eps': data.get('EPS', ''),
            'dividend': data.get('DividendPerShare', ''),
            'dividendYield': data.get('DividendYield', ''),
            'exchange': data.get('Exchange', ''),
            '52WeekHigh': data.get('52WeekHigh', ''),
            '52WeekLow': data.get('52WeekLow', ''),
        }
    return result   

def get_new_stories(symbol, max_stories):
    request_url = f"https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&limit={max_stories}&apikey={ALPHA_VANTAGE_API_KEY}"
    response = requests.get(request_url)
    if response.status_code == 200:
        data = response.json()
        result = []
        feed = data["feed"]
        for record in feed:
            news_item = { "title" : record["title"], "url" : record["url"], "summary" : record["summary"], "published" : record["time_published"] }
            result.append(news_item)
            if len(result) >= max_stories: 
                break
        return result 
    else:
        return None

def get_prices(stock_symbol):  
    url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={stock_symbol}&apikey={ALPHA_VANTAGE_API_KEY}'    
    response = requests.get(url)
    data = response.json()
    days = data["Time Series (Daily)"]
    keys = sorted(days.keys(),reverse=True)
    daily= {"current": days[keys[0]]["4. close"], 
            "volume": days[keys[0]]["5. volume"], 
            "previous": days[keys[1]]["4. close"], 
            "open": days[keys[0]]["1. open"]}
    return daily

def get_historical_closing_prices(stock_symbol,number_of_days):
    url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={stock_symbol}&outputsize=full&apikey={ALPHA_VANTAGE_API_KEY}'    
    response = requests.get(url)
    data = response.json()
    days = data["Time Series (Daily)"]
    keys = sorted(days.keys(),reverse=True)
    x = []
    y = []
    count = 0    
    for date in keys:
        x.append(date)
        y.append(days[date]["5. adjusted close"])
        count += 1        
        if count >= number_of_days:
            break    
    return { "x": x, "y": y}


if __name__ == '__main__':
    app = Flask(__name__)
    app.register_blueprint(bp)
    app.run(debug=True, port=8080)
